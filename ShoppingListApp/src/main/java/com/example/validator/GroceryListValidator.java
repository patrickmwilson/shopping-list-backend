package com.example.validator;

import com.example.model.GroceryList;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class GroceryListValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return GroceryList.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty", "Grocery List must have a name");
    }
    
}
