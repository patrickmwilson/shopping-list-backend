package com.example.repository;

import com.example.model.GroceryItem;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GroceryItemRepository extends JpaRepository<GroceryItem, Integer> {
    
    GroceryItem findByGroceryItemId(int groceryItemId);
}
