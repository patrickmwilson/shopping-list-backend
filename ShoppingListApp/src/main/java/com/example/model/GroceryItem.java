package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="grocery_item")
public class GroceryItem {

    @Id
    @Column(name="grocery_item_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.NONE)
    private int groceryItemId;

    @Column(name="name")
    private String name;

    @Column(name="type")
    private String type;

    @Column(name="quantity")
    private int quantity;

    public GroceryItem(String name, String type, int quantity) {
        super();
        this.name = name;
        this.type = type;
        this.quantity = quantity;
    }
    
}
