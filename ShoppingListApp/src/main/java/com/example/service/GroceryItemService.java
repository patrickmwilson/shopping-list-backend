package com.example.service;

import com.example.model.GroceryItem;
import com.example.repository.GroceryItemRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@NoArgsConstructor
public class GroceryItemService {

    public GroceryItemRepository itemRepo;

    public GroceryItem save(GroceryItem groceryItem) {
        return itemRepo.save(groceryItem);
    }

    public void delete(int groceryItemId) {
        GroceryItem gItem = findById(groceryItemId);
        itemRepo.delete(gItem);
    }

    public GroceryItem findById(int groceryItemId) {
        return itemRepo.findByGroceryItemId(groceryItemId);
    }
}
