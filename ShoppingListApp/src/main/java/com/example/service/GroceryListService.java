package com.example.service;

import java.util.List;

import com.example.model.GroceryList;
import com.example.repository.GroceryListRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@NoArgsConstructor
public class GroceryListService {

    public GroceryListRepository listRepo;

    public List<GroceryList> findAll(){
        return listRepo.findAll();
    }

    public GroceryList save(GroceryList groceryList) {
        return listRepo.save(groceryList);
    }

    public void delete(int groceryListId) {
        GroceryList gList = findById(groceryListId);
        listRepo.delete(gList);
    }

    public GroceryList findById(int groceryListId) {
        return listRepo.findByGroceryListId(groceryListId);
    }
    
}
