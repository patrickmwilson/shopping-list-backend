package com.example.controller;

import java.util.LinkedHashMap;
import java.util.List;

import com.example.model.GroceryItem;
import com.example.model.GroceryList;
import com.example.service.GroceryItemService;
import com.example.service.GroceryListService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/grocery-lists")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@NoArgsConstructor
public class GroceryListController {

    private GroceryListService listServ;
    private GroceryItemService itemServ;

    @GetMapping("/all")
    public ResponseEntity<List<GroceryList>> getAll() {
        List<GroceryList> gList = listServ.findAll();
        return new ResponseEntity<>(gList, HttpStatus.OK);
    }

    @GetMapping("/{groceryListId}")
    public ResponseEntity<GroceryList> getById(@PathVariable("groceryListId") Integer groceryListId) {
        GroceryList gList = listServ.findById(groceryListId);
        return new ResponseEntity<>(gList, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<GroceryList> insertGroceryList(@RequestBody GroceryList groceryList) {
        GroceryList gList = listServ.save(groceryList);
        return new ResponseEntity<>(gList, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{groceryListId}")
    public ResponseEntity<String> deleteGroceryList(@PathVariable("groceryListId") Integer groceryListId) {
        listServ.delete(groceryListId);
        return new ResponseEntity<>("Resource deleted", HttpStatus.OK);
    }

    @PostMapping("/items")
    public ResponseEntity<GroceryItem> insertGroceryItem(@RequestBody LinkedHashMap<String, String> reqMap) {
        GroceryList gList = listServ.findById(Integer.parseInt(reqMap.get("groceryListId")));
        GroceryItem gItem = new GroceryItem(reqMap.get("name"), reqMap.get("type"), Integer.parseInt(reqMap.get("quantity")));
        gItem = itemServ.save(gItem);

        List<GroceryItem> iList = gList.getItemList();
        iList.add(gItem);
        gList.setItemList(iList);
        listServ.save(gList);

        return new ResponseEntity<>(gItem, HttpStatus.OK);
    }

    @DeleteMapping("/items/{itemId}")
    public ResponseEntity<String> deleteGroceryItem(@PathVariable("itemId") Integer groceryItemId) {
        itemServ.delete(groceryItemId);
        return new ResponseEntity<>("Resource deleted", HttpStatus.OK);
    }
    
}
